﻿using System.Collections.Generic;

namespace BalancedBrackets
{
	public static class Code
	{
		public static string IsBalanced(string brackets)
		{
			Stack<char> s = new Stack<char>();
			foreach (char c in brackets.ToCharArray())
			{
				if (c == '[') s.Push(']');
				else if (c == '(') s.Push(')');
				else if (c == '{') s.Push('}');
				else
				{
					if (s.Count == 0 || c != s.Peek())
						return "NO";
					s.Pop();
				}
			}
			return s.Count == 0 ? "YES" : "NO";
		}
	}
}
