﻿using System;

namespace BalancedBrackets
{
	class Program
	{
		static void Main(string[] args)
		{
			string one = "}][}}(}][))]";
			string two = "[](){()}";
			string three = "()";
			string four = "({}([][]))[]()";
			string five = "{)[](}]}]}))}(())(";

			Console.WriteLine(Code.IsBalanced(one));
			Console.WriteLine(Code.IsBalanced(two));
			Console.WriteLine(Code.IsBalanced(three));
			Console.WriteLine(Code.IsBalanced(four));
			Console.WriteLine(Code.IsBalanced(five));
		}
	}
}
