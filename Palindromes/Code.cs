﻿using System;

namespace Palindromes
{
	public static class Code
	{
		public static bool IsPalindrome(string strToTest)
		{
			var beginning = strToTest.Substring(0, strToTest.Length / 2);
			var end = strToTest.Substring((strToTest.Length % 2 == 0) ? strToTest.Length / 2 : (strToTest.Length / 2) + 1);
			if (beginning == ReverseString(end))
			{
				return true;
			}
			return false;
		}

		private static string ReverseString(string strToReverse)
		{
			if (string.IsNullOrEmpty(strToReverse)) return strToReverse;
			var reversed = string.Empty;
			for (int i = strToReverse.Length - 1; i >= 0; i--)
			{
				reversed += strToReverse.ToCharArray()[i].ToString();
			}
			return reversed;
		}
	}
}
