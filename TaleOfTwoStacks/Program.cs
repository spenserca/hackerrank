﻿using System;
using System.Collections.Generic;

namespace TaleOfTwoStacks
{
	class Program
	{
		static void Main(string[] args)
		{
			var queue = new Queue<string>();
			var commands = Convert.ToInt32(Console.ReadLine());
			for (int i = 0; i < commands; i++)
			{
				var process = Console.ReadLine().Split(' ');
				switch (process[0])
				{
					case "1":
						queue.Enqueue(process[1]);
						break;
					case "2":
						queue.Dequeue();
						break;
					case "3":
						Console.WriteLine(queue.Peek());
						break;
				}
			}
		}
	}
}
