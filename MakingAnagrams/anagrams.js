console.log(CountOfDeletionsToMakeAnagram("cde", "abc"));

function CountOfDeletionsToMakeAnagram(first, second) {
    var letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    var temp = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    var deleted = 0;
    first.split("").forEach(currentItem => {
        var letterIndex = letters.findIndex(c => c === currentItem);
        if(letterIndex != -1){
            temp[letterIndex] += 1;
        }
    });
    second.split("").forEach(currentItem => {
        var tempIndex = letters.findIndex(c => c === currentItem);
        if(tempIndex != -1){
            temp[tempIndex] -= 1;
        }
    });
    temp.forEach(i => {
        deleted += Math.abs(i);
    });
    return deleted;
}