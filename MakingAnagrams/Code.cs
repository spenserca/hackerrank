﻿using System;

namespace MakingAnagrams
{
	public static class Code
	{
		public static int GetCountOfDeletionsToMakeAnagram(string one, string two)
		{
			int[] letters = new int[26]; // all values = 0
			int deleted = 0;

			foreach (char c in one.ToCharArray())
			{
				letters[c - 'a']++; // increment the value at index c - 'a' (99)
			}
			foreach (char c in two.ToCharArray())
			{
				letters[c - 'a']--; // decrement the value at index c - 'a' (99)
			}
			foreach (int i in letters)
			{
				deleted += Math.Abs(i);
			}
			return deleted;
		}
	}
}
