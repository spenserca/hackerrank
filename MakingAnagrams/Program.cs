﻿using System;

namespace MakingAnagrams
{
	class Program
	{
		static void Main(string[] args)
		{
			string inputOne = "cde";
			string inputTwo = "abc";

			Console.WriteLine(Code.GetCountOfDeletionsToMakeAnagram(inputOne, inputTwo));
		}
	}
}
