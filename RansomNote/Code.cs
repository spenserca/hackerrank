﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RansomNote
{
	public static class Code
	{
		public static bool CanWriteRansomNote(string[] mags, string[] note)
		{
			Array.Sort(mags);
			Array.Sort(note);

			List<string> magList = mags.ToList();
			foreach (var word in note)
				if (!magList.Remove(word)) return false;

			return true;
		}
	}
}
