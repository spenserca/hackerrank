﻿using System;
using System.Collections.Generic;


namespace RansomNote
{
	class Program
	{
		static void Main(string[] args)
		{
			int[] tokens = new[] { 6, 4 };
			string[] magazine = new string[] { "give", "me", "one", "grand", "today", "tonight" };
			string[] note = new string[] { "give", "one", "grand", "today" };

			Console.WriteLine(Code.CanWriteRansomNote(magazine, note) ? "Yes" : "No");
		}
	}
}
