﻿namespace CoinChange
{
	public static class Code
	{
		public static long MakeChange(int[] coins, int money)
		{
			var dp = new long[money + 1];
			dp[0] = 1; // if money = 0, result is 1
			foreach (var coin in coins)
			{
				for (int i = coin; i < dp.Length; i++)
				{
					dp[i] += dp[i - coin];
				}
			}

			return dp[money];
		}
	}
}
