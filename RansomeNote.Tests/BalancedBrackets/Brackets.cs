﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BalancedBrackets;

namespace RansomeNote.Tests.BalancedBrackets
{
	[TestClass]
	public class Brackets
	{
		private string brackets;

		[TestMethod]
		public void BalancedBracket()
		{
			brackets = "{[()]}";

			Assert.AreEqual("YES", Code.IsBalanced(brackets));
		}

		[TestMethod]
		public void UnBalancedBracket()
		{
			brackets = "{[(])}";

			Assert.AreEqual("NO", Code.IsBalanced(brackets));
		}
	}
}
