﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MakingAnagrams;

namespace RansomeNote.Tests.MakingAnagrams
{
	[TestClass]
	public class AnagramCount
	{
		private string one;
		private string two;

		[TestMethod]
		public void GetAnagramCount()
		{
			one = "cde";
			two = "abc";

			Assert.AreEqual(4, Code.GetCountOfDeletionsToMakeAnagram(one, two));
		}
	}
}
