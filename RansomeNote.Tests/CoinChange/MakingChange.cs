﻿using CoinChange;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RansomeNote.Tests.CoinChange
{
	[TestClass]
	public class MakingChange
	{
		[TestMethod]
		public void MakeChangeFromCoins()
		{
			// num dollars = 4
			// number of distinct coins
			// values of distinct coins
			var coins = new[] { 1, 2, 3 };
			Assert.AreEqual(4, Code.MakeChange(coins, 4));

			coins = new[] { 2, 5, 3, 6 };
			Assert.AreEqual(5, Code.MakeChange(coins, 10));
		}
	}
}
