﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RansomNote;

namespace RansomeNote.Tests
{
	[TestClass]
	public class CanWriteRansomNote
	{
		private string[] _mags = new string[] { "give", "me", "one", "grand", "today", "tonight" };
		private string[] _note = new string[] { "give", "one", "grand", "today" };

		[TestMethod]
		public void CanWriteRansomListTwo()
		{
			bool canWriteNote = Code.CanWriteRansomNote(_mags, _note);

			Assert.AreEqual(true, canWriteNote);
		}
	}
}