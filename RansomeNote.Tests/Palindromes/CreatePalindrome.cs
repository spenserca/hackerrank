﻿using Palindromes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RansomeNote.Tests.Palindromes
{
	[TestClass]
	public class CreatePalindrome
	{
		private string testString;

		[TestMethod]
		public void NoSwapsToCreatePalindrome()
		{
			testString = "b";
			Assert.AreEqual(true, Code.IsPalindrome(testString));

			testString = "bb";
			Assert.AreEqual(true, Code.IsPalindrome(testString));

			testString = "abcba";
			Assert.AreEqual(true, Code.IsPalindrome(testString));
		}

		[TestMethod]
		public void CountSwapsToCreatePalindrome()
		{
			testString = "abb";
			Assert.AreEqual(false, Code.IsPalindrome(testString));

			testString = "cbaabbb";
			Assert.AreEqual(false, Code.IsPalindrome(testString));
		}
	}
}
