﻿using LeftRotation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace RansomeNote.Tests.LeftRotation
{
	[TestClass]
	public class Rotation
	{
		private string[] numsToRotate;
		private string[] numsRots;
		private int numCount;
		private int rotations;
		private string rotatedNums;

		[TestMethod]
		public void LeftRotateArrayEfficient()
		{
			numsToRotate = new[] { "1", "2", "3", "4", "5" };
			numsRots = new[] { "5", "4" };
			numCount = Convert.ToInt32(numsRots[0]);
			rotations = Convert.ToInt32(numsRots[1]);

			rotatedNums = Code.LeftRotateArrayEfficient(numsToRotate, rotations);

			Assert.AreEqual("5 1 2 3 4", rotatedNums);
		}
	}
}
