﻿using System;

namespace LeftRotation
{
	class Program
	{
		static void Main(string[] args)
		{
			string[] numIntsRots = new[] { "5", "4" };
			int intCount = Convert.ToInt32(numIntsRots[0]);
			int rots = Convert.ToInt32(numIntsRots[1]);
			string[] numbers = new[] { "1", "2", "3", "4", "5" };

			Console.Write(Code.LeftRotateArrayEfficient(numbers, rots) + Environment.NewLine);
		}
	}
}
